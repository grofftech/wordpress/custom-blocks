/* eslint-disable no-console */
/* eslint-disable jsx-a11y/click-events-have-key-events */
const { __ } = wp.i18n;
const { MediaUpload, MediaUploadCheck, RichText, InspectorControls, ColorPalette, BlockControls, AlignmentToolbar } = wp.blockEditor;
const { registerBlockType } = wp.blocks;
const { Button, PanelBody, PanelRow } = wp.components;

registerBlockType( "grofftech/call-to-action", {
    title: "Call To Action",
    description: "Creates a full width background image with a call to action button",
    icon: "heart",
    category: "common",

    attributes: {
        headingText: {
            type: "array",
            source: "children",
            selector: ".call-to-action__heading-text",
        },
        buttonText: {
            type: "array",
            source: "children",
            selector: ".call-to-action__button-text",
        },
        fontColor: {
            type: "string",
            default: "black",
        },
        overlayColor: {
            type: "string",
            default: "none",
        },
        backgroundImage: {
            type: "string",
            default: null,
        },
        alignment: {
            type: "string",
        },
    },

    edit: ( props ) => {
        const {
            className,
            attributes: { headingText, buttonText, fontColor, overlayColor, backgroundImage, alignment },
            setAttributes,
        } = props;

        const alignmentClass = alignment ? " align-" + alignment : "";

        const onImageSelect = ( imageObject ) => {
            setAttributes( {
                backgroundImage: imageObject.sizes.full.url,
            } );
        };

        const onHeadingTextChange = ( changed ) => {
            setAttributes( {
                headingText: changed,
            } );
        };

        const onButtonTextChange = ( changed ) => {
            setAttributes( {
                buttonText: changed,
            } );
        };

        const onTextColorChange = ( colorChange ) => {
            setAttributes( {
                fontColor: colorChange,
            } );
        };

        const onOverlayColorChange = ( colorChange ) => {
            setAttributes( {
                overlayColor: colorChange,
            } );
        };

        return (
            <>
                <InspectorControls>
                    <PanelBody title={ __( "Font Color" ) }>
                        <PanelRow>
                            <ColorPalette
                                value={ fontColor }
                                onChange={ onTextColorChange }
                            />
                        </PanelRow>
                    </PanelBody>
                    <PanelBody title={ __( "Overlay Color" ) }>
                        <PanelRow>
                            <ColorPalette
                                value={ overlayColor }
                                onChange={ onOverlayColorChange }
                            />
                        </PanelRow>
                    </PanelBody>
                    <PanelBody title={ __( "Background Image" ) }>
                        <PanelRow>
                            <MediaUploadCheck>
                                <MediaUpload
                                    onSelect={ onImageSelect }
                                    type="image"
                                    value={ backgroundImage }
                                    render={ ( { open } ) => (
                                        <Button
                                            className="is-secondary"
                                            onClick={ open }>
                                            <span>Upload Image</span>
                                        </Button>
                                    ) }
                                />
                            </MediaUploadCheck>
                        </PanelRow>
                    </PanelBody>
                </InspectorControls>
                <BlockControls>
                    <AlignmentToolbar
                        value={ alignment }
                        onChange={ ( alignChange ) => {
                            console.log( alignChange );
                            setAttributes( { alignment: alignChange } );
                        } }
                    />
                </BlockControls>
                <div
                    className={ className }
                    style={ {
                        backgroundImage: `url( ${ backgroundImage } )`,
                        backgroundSize: `cover`,
                        backgroundPosition: `center`,
                        height: `180px`,
                    } }
                >
                    <div
                        className="call-to-action__overlay"
                        style={ { backgroundColor: overlayColor } }>
                    </div>
                    <div
                        className="call-to-action__container"
                    >
                        <RichText
                            tagName="p"
                            className={ `call-to-action__heading-text${ alignmentClass }` }
                            value={ headingText }
                            onChange={ onHeadingTextChange }
                            placeholder="Enter text here"
                            keepPlaceholderOnFocus={ true }
                            style={ { color: fontColor } }
                        />
                        <button className={ `btn btn-primary btn-medium${ alignmentClass }` }>
                            <RichText
                                tagName="span"
                                className="call-to-action__button-text"
                                value={ buttonText }
                                onChange={ onButtonTextChange }
                                placeholder="Enter button text"
                                keepPlaceholderOnFocus={ false }
                            />
                        </button>
                    </div>
                </div>
            </>
        );
    },
    save: ( props ) => {
        const {
            className,
            attributes: { backgroundImage, overlayColor, headingText, buttonText, fontColor, alignment },
        } = props;

        const alignmentClass = alignment ? " align-" + alignment : "";

        return (
            <div
                className={ className }
                style={ {
                    backgroundImage: `url( ${ backgroundImage } )`,
                    backgroundSize: `cover`,
                    backgroundPosition: `center`,
                    height: `180px`,
                } }
            >
                <div
                    className="call-to-action__overlay"
                    style={ { backgroundColor: overlayColor } }>
                </div>
                <div
                    className="call-to-action__container"
                >
                    <RichText.Content
                        tagName="p"
                        className={ `call-to-action__heading-text${ alignmentClass }` }
                        value={ headingText }
                        style={ { color: fontColor } }
                    />
                    <button className={ `btn btn-primary btn-medium${ alignmentClass }` }>
                        <RichText.Content
                            tagName="span"
                            className="call-to-action__button-text"
                            value={ buttonText }
                            style={ { color: fontColor } }
                        />
                    </button>
                </div>
            </div>
        );
    },
} );
