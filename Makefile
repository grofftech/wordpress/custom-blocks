# Executables
SASS=node_modules/.bin/sass
POST_CSS=node_modules/.bin/postcss
STYLELINT=node_modules/.bin/stylelint
WATCH=node_modules/.bin/watch
ESLINT=node_modules/.bin/eslint
WPSCRIPTS=node_modules/.bin/wp-scripts

# Configuration
CONFIG_DIR=config
STYLELINT_CONFIG=$(CONFIG_DIR)/stylelintrc.json
ESLINT_CONFIG=$(CONFIG_DIR)/.eslintrc.json
MAKE_OPTIONS=--no-print-directory

# Sources
SOURCE_DIR=assets/src
JS_FILE=$(SOURCE_DIR)/blocks/index.js
CSS_SOURCE_DIR=$(SOURCE_DIR)/sass
JS_SOURCE_DIR=$(SOURCE_DIR)/blocks/**/*.js
SASS_FILES=$(SOURCE_DIR)/blocks/**/*.scss

# Distribution
DIST_DIR=assets/dist

all: clean compile

compile: stylelint lint-js
	@ echo "Compiling JS files..."
	@ $(WPSCRIPTS) build $(JS_FILE) \
		--output-path=$(DIST_DIR)

stylelint:
	@ echo "Linting Sass..."
	@ $(STYLELINT) $(SASS_FILES) \
		--config $(STYLELINT_CONFIG)

lint-js:
	@ echo "Linting JS..."
	@ $(ESLINT) \
		--config $(ESLINT_CONFIG) \
		$(JS_SOURCE_DIR)

clean:
	@ echo "Removing directory '$(DIST_DIR)'..."
	@ rm -rf $(DIST_DIR)

.PHONY: all clean compile stylelint lint-js
