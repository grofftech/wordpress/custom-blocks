<?php
/**
 * Custom Blocks plugin.
 *
 * @package     Grofftech\CustomBlocks
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @copyright   2016 Grofftech
 * @license     GNU General Public License 2.0+
 *
 * @wordpress-plugin
 * Plugin Name: Custom Blocks
 * Plugin URI:  https://
 * Description: Custom Blocks for the WordPress Block Editor
 * Version:     1.0.0
 * Author:      Grofftech
 * Author URI:  https://grofftech.net
 * Text Domain: custom_blocks
 * License:     GPL-2.0+
 * License URI: http: //www.gnu.org/licenses/gpl-2.0.txt
 * Requires PHP: 7.2 or higher
 */

namespace Grofftech\CustomBlocks;

use Grofftech\CustomBlocks\Admin\Notification\Notification;
use Grofftech\CustomBlocks\Dependencies\Auryn\Injector;

if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Not a valid WordPress installation!' );
}

$plugin_path = \plugin_dir_path( __FILE__ );
$plugin_url = \plugin_dir_url( __FILE__ );

// Import and create the notification class.
require_once $plugin_path . 'lib/Admin/Notification/Notification.php';
$notification = new Notification();

if ( version_compare( PHP_VERSION, '7.2.0' ) <= 0 ) {
    $notification->show_error_message( 'Custom Blocks requires PHP version 7.2.0 or higher.' );

    deactivate_plugin();
}

// Autoload class files
$composer_autoload = "{$plugin_path}/vendor/autoload.php";

if ( ! file_exists( $composer_autoload ) ) {
    $notification->show_error_message( "Failed to load Custom Blocks. Did you remember to run composer install?" );

    deactivate_plugin();
    return;
}

require $composer_autoload;

try {
    global $custom_blocks;

    $custom_blocks = new CustomBlocks(
        $plugin_path,
        $plugin_url,
        new Injector()
    );

    \add_action( 'plugins_loaded', array( $custom_blocks, 'run' ) );

    \register_activation_hook(
        __FILE__,
        'Grofftech\CustomBlocks\CustomBlocks::activate'
    );

    \register_deactivation_hook(
        __FILE__,
        'Grofftech\CustomBlocks\CustomBlocks::deactivate'
    );

    \register_uninstall_hook(
        __FILE__,
        'Grofftech\CustomBlocks\CustomBlocks::uninstall'
    );
} catch ( \Throwable $e ) {
    $notification->show_error_message( 'Failed to activate the Custom Blocks plugin. Did you remember to run composer install?' );

    deactivate_plugin();
}

/**
 * Deactivates the plugin.
 *
 * @since 1.0.0
 *
 * @return void
 */
function deactivate_plugin() {
    // This will hide the plugin activation notification
    unset( $_GET['activate'] );

    // Deactivate plugin
    \add_action( 'admin_init', function () {
        deactivate_plugins( __FILE__ );
	} );
}
