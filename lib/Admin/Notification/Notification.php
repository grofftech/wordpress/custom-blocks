<?php
/**
 * Notification Handler
 *
 * @package     Grofftech\MetaBoxGenerator\Admin
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\CustomBlocks\Admin\Notification;

/**
 * Notification class.
 */
class Notification {

    /**
     * Constructor.
     */
    public function __construct() {
    }

    /**
     * Show error message.
     *
     * @since 1.0.0
     *
     * @param string $message The message to show.
     *
     * @return void
     */
    public function show_error_message( $message ) {
        // Add a custom action so we can use the message parameter.
        add_action( 'custom_blocks_error_message', array( $this, 'render_error_message' ), 10, 1 );

        // Do the custom action with the message.
		add_action(
            'admin_notices',
            function() use ( $message ) {
				do_action( 'custom_blocks_error_message', $message );
			}
        );
    }

    /**
     * Render the error message by loading viw file.
     *
     * @since 1.0.0
     *
     * @param string $message The message to display.
     *
     * @return void
     */
    public function render_error_message( $message ) {
        include __DIR__ . '/Views/Error.php';
    }
}
