<?php
/**
 * Block Service
 *
 * @package     Grofftech\CustomBlocks\Services\Block
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\CustomBlocks\Block;

use Grofftech\CustomBlocks\Service\Service;

/**
 * Block class.
 */
class Block extends Service {

    /**
     * The block asset file.
     *
     * @var string
     */
    private $asset_file;

    /**
     * Constructor.
     */
    public function __construct() {
        $this->asset_file = include CUSTOM_BLOCKS_DIR . 'assets/dist/index.asset.php';
    }

    /**
     * Run class dependencies. Overrides the run method of parent.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->register_hooks();
    }

    /**
     * Register hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {
        \add_action( 'enqueue_block_assets', array( $this, 'add_block_assets' ) );
        \add_action( 'enqueue_block_editor_assets', array( $this, 'add_block_editor_assets' ) );
    }

    /**
     * Adds assets for the back-end block editor and front end styles.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function add_block_assets() {
        $this->add_styles();
    }

    /**
     * Enqueue's styles meant for the back-end and front-end.
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function add_styles() {
        \wp_enqueue_style(
            'custom-block-styles',
            CUSTOM_BLOCKS_URL . 'assets/dist/style-index.css',
            array(),
            $this->asset_file['version']
        );
    }

    /**
     * Adds assets meant for the back-end editor only.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function add_block_editor_assets() {
        $this->add_editor_styles();
        $this->add_editor_scripts();
    }

    /**
     * Adds styles for use with the back-end editor.
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function add_editor_styles() {
        \wp_enqueue_style(
            'custom-block-styles-editor',
            CUSTOM_BLOCKS_URL . 'assets/dist/index.css',
            array(),
            $this->asset_file['version']
        );
    }

    /**
     * Adds scripts for use with the back-end editor.
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function add_editor_scripts() {
        \wp_enqueue_script(
            'custom-blocks',
            CUSTOM_BLOCKS_URL . 'assets/dist/index.js',
            array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ),
            $this->asset_file['version'],
            false
        );
    }
}
