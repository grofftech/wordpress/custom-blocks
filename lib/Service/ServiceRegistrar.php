<?php
/**
 * Registers services required for the plugin.
 *
 * @package     Grofftech\CustomBlocks\Services
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\CustomBlocks\Service;

use Grofftech\CustomBlocks\Interfaces\Runnable;

/**
 * Service Registrar abstract class
 */
abstract class ServiceRegistrar implements Runnable {
    /**
     * The classes to instantiate
     *
     * @var array
     */
    protected $classes = array();


    /**
     * The auryn dependency injector.
     *
     * @var Auryn
     */
    protected $injector;

    /**
     * Kick off the registration.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->register_services();
    }

    /**
     * Register the services and call the run methods
     * for each service.
     *
     * @since 1.0.0
     *
     * @return void
     */
    protected function register_services() {
        $this->services = $this->init_classes();

        foreach ( $this->services as $service ) {
            $service->run();
        }
    }

    /**
     * Initialize/Instantiate the classes.
     *
     * @since 1.0.0
     *
     * @return array
     */
    protected function init_classes() {
        $objects = array_map(
            function( $class ) {
					return array(
						'namespace' => $class,
						'object' => $this->injector->make( $class ),
					);
			},
            $this->classes
        );

        return array_column( $objects, 'object', 'namespace' );
    }
}
