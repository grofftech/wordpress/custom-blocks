<?php
/**
 * Service
 *
 * @package     Grofftech\CustomBlocks\Service
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\CustomBlocks\Service;

use Grofftech\CustomBlocks\Interfaces\Hookable;
use Grofftech\CustomBlocks\Interfaces\Runnable;

/**
 * Service abstract class.
 */
abstract class Service implements Runnable, Hookable {

    /**
     * Kick off service functionality.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->register_hooks();
    }
}
