<?php
/**
 * Custom Blocks Plugin Handler
 *
 * @package     Grofftech\CustomBlocks\CustomBlocks
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\CustomBlocks;

use Grofftech\CustomBlocks\Block\Block;
use Grofftech\CustomBlocks\Service\ServiceRegistrar;

/**
 * Custom Blocks class.
 */
class CustomBlocks extends ServiceRegistrar {

    /**
     * The plugin directory
     *
     * @var string
     */
    private $plugin_path;

    /**
     * The plugin url
     *
     * @var string
     */
    private $plugin_url;

    /**
     * The auryn dependency injector
     *
     * @var Grofftech\CustomBlocks\Dependencies\Auryn
     */
    protected $injector;

    /**
     * The services/classes to instantiate. Overrides property in parent.
     *
     * @var array
     */
    protected $classes = array(
        Block::class,
    );

    /**
     * Constructor
     *
     * @param  string $plugin_path  The plugin path.
     * @param  string $plugin_url   The plugin url.
     * @param  Grofftech\CustomBlocks\Dependencies\Auryn  $injector The dependency injector.
     */
    public function __construct( $plugin_path, $plugin_url, $injector ) {
        $this->plugin_path = $plugin_path;
        $this->plugin_url = $plugin_url;
        $this->injector = $injector;
    }

    /**
     * Kicks off plugin functionality.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->init_constants();
        $this->register_hooks();
        parent::run();
    }

    /**
     * Initialize plugin constants
     *
     * @since 1.0.0
     *
     * @return void
     */
    private function init_constants() {
		if ( ! defined( 'CUSTOM_BLOCKS_DIR' ) ) {
            define( 'CUSTOM_BLOCKS_DIR', $this->plugin_path );
		}

		if ( ! defined( 'CUSTOM_BLOCKS_URL' ) ) {
			define( 'CUSTOM_BLOCKS_URL', $this->plugin_url );
		}

		if ( ! defined( 'CUSTOM_BLOCKS_CONFIG_DIR' ) ) {
			define( 'CUSTOM_BLOCKS_CONFIG_DIR', CUSTOM_BLOCKS_DIR . '/config/' );
		}

		if ( ! defined( 'CUSTOM_BLOCKS_NAME' ) ) {
			define( 'CUSTOM_BLOCKS_NAME', 'Custom Blocks' );
		}

		if ( ! defined( 'CUSTOM_BLOCKS_VERSION' ) ) {
            define( 'CUSTOM_BLOCKS_VERSION', '1.0.0' );
		}
    }

    /**
     * Registers plugin level hooks.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register_hooks() {

    }

        /**
     * Runs anything that needs to be done when
     * the plugin is activated.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function activate() {

    }

    /**
     * Runs anything that needs to be done when
     * the plugin is deactivated.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function deactivate() {

    }

    /**
     * Runs anything that needs to be done
     * when the plugin is uninstalled.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function uninstall() {

    }

    /**
     * Gets a specific class instance.
     *
     * @since 1.0.0
     *
     * @param string $classname The namespace with class name.
     *
     * @return object
     */
    public function get_class( string $classname ) {
        return (object) $this->classes[ $classname ];
    }
}
