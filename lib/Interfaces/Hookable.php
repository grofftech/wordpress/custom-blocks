<?php
/**
 * Hookable Interface
 *
 * @package     Grofftech\CustomBlocks\Interfaces
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\CustomBlocks\Interfaces;

/**
 * Hookable interface.
 */
interface Hookable {

    /**
     * Register hooks method.
     */
    public function register_hooks();
}
